﻿using Clockwork.API.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Controllers
{
    public class TimeZoneController : Controller
    {
        [EnableCors]
        [HttpGet]
        [Route("api/TimeZone/Get")]
        public IActionResult Get()
        {
            //ideally this would come from a database table.
            var timezones = new List<TimeZoneQueries>();

            using (var db = new ClockworkContext())
            {
                timezones = db.TimeZoneQueries.ToList();
            }

            return Ok(timezones);
        }

        [EnableCors]
        [HttpGet]
        [Route("api/TimeZone/GetCurrenTimeByTimeZone/{timeZoneId}/")]
        public IActionResult GetCurrenTimeByTimeZone(int timeZoneId)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            

            using (var db = new ClockworkContext())
            {
                var timeZoneRecord = db.TimeZoneQueries.Where(x => x.TimeZoneQueryId == timeZoneId).FirstOrDefault();

                if (timeZoneRecord != null)
                {
                   

                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneRecord.Label);
                    DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);

                    var returnVal = new CurrentTimeZoneQueries
                    {
                        UTCTime = utcTime,
                        ClientIp = ip,
                        ServerTime = serverTime,
                        TimeZoneTime = localTime,
                        TimeZoneId = timeZoneId
                     };

                    db.CurrentTimeZoneQueries.Add(returnVal);
                    var count = db.SaveChanges();
                    Console.WriteLine("{0} records saved to database", count);

                    return Ok(returnVal);
                }
                else
                {
                    return BadRequest();
                }
                

            }

          
        }
    }
}
