﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using Microsoft.AspNetCore.Cors;
using System.Linq;
using System.Collections.Generic;

namespace Clockwork.API.Controllers
{
    
    public class CurrentTimeController : Controller
    {
        // GET api/currenttime
        [EnableCors]
        [HttpGet]
        [Route("api/CurrentTime/Get/")]
        public IActionResult Get()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime
            };

            using (var db = new ClockworkContext())
            {

                    db.CurrentTimeQueries.Add(returnVal);
                    var count = db.SaveChanges();
                    Console.WriteLine("{0} records saved to database", count);

                    Console.WriteLine();
                    foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                    {
                        Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                    }

                
            }

            return Ok(returnVal);
        }

        [EnableCors]
        [HttpGet]
        [Route("api/CurrentTime/GetAll")]
        public IActionResult GetAll()
        {
            var allRecords = new List<CurrentTimeQuery>();

            using (var db = new ClockworkContext())
            {
                allRecords = db.CurrentTimeQueries.ToList();
            }

            return Ok(allRecords);
        }
    }
}
