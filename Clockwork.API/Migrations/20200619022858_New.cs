﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Clockwork.API.Migrations
{
    public partial class New : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CurrentTimeQueries",
                columns: table => new
                {
                    CurrentTimeQueryId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Time = table.Column<DateTime>(nullable: false),
                    ClientIp = table.Column<string>(nullable: true),
                    UTCTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrentTimeQueries", x => x.CurrentTimeQueryId);
                });

            migrationBuilder.CreateTable(
                name: "CurrentTimeZoneQueries",
                columns: table => new
                {
                    CurrentTimeZoneQueryId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ServerTime = table.Column<DateTime>(nullable: false),
                    TimeZoneId = table.Column<int>(nullable: false),
                    TimeZoneTime = table.Column<DateTime>(nullable: false),
                    UTCTime = table.Column<DateTime>(nullable: false),
                    ClientIp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrentTimeZoneQueries", x => x.CurrentTimeZoneQueryId);
                });

            migrationBuilder.CreateTable(
                name: "TimeZoneQueries",
                columns: table => new
                {
                    TimeZoneQueryId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Label = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeZoneQueries", x => x.TimeZoneQueryId);
                });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 1, "Dateline Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 102, "N. Central Asia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 101, "North Asia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 100, "W. Mongolia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 99, "Altai Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 98, "SE Asia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 97, "Myanmar Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 96, "Omsk Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 95, "Bangladesh Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 94, "Central Asia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 93, "Nepal Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 92, "Sri Lanka Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 91, "India Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 90, "Qyzylorda Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 89, "Pakistan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 88, "Ekaterinburg Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 87, "West Asia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 86, "Afghanistan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 72, "Arab Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 73, "Belarus Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 74, "Russian Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 75, "E. Africa Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 76, "Iran Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 77, "Arabian Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 103, "Tomsk Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 78, "Astrakhan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 80, "Russia Time Zone 3" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 81, "Mauritius Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 82, "Saratov Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 83, "Georgian Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 84, "Volgograd Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 85, "Caucasus Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 79, "Azerbaijan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 71, "Turkey Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 104, "China Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 106, "Singapore Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 137, "Tonga Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 136, "UTC+13" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 135, "Chatham Islands Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 134, "Kamchatka Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 133, "Fiji Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 132, "UTC+12" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 131, "New Zealand Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 130, "Russia Time Zone 11" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 129, "Central Pacific Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 128, "Sakhalin Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 127, "Norfolk Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 126, "Magadan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 125, "Russia Time Zone 10" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 124, "Bougainville Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 123, "Lord Howe Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 122, "Vladivostok Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 121, "Tasmania Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 107, "W. Australia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 108, "Taipei Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 109, "Ulaanbaatar Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 110, "Aus Central W. Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 111, "Transbaikal Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 112, "Tokyo Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 105, "North Asia East Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 113, "North Korea Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 115, "Yakutsk Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 116, "Cen. Australia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 117, "AUS Central Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 118, "E. Australia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 119, "AUS Eastern Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 120, "West Pacific Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 114, "Korea Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 138, "Samoa Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 70, "Arabic Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 68, "Libya Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 32, "Newfoundland Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 31, "Pacific SA Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 30, "SA Western Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 29, "Central Brazilian Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 28, "Venezuela Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 27, "Atlantic Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 26, "Paraguay Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 25, "Turks And Caicos Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 24, "US Eastern Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 23, "Cuba Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 22, "Haiti Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 21, "Eastern Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 20, "Eastern Standard Time (Mexico)" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 19, "SA Pacific Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 18, "Canada Central Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 17, "Central Standard Time (Mexico)" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 16, "Easter Island Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 2, "UTC-11" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 3, "Aleutian Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 4, "Hawaiian Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 5, "Marquesas Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 6, "Alaskan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 7, "UTC-09" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 33, "Tocantins Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 8, "Pacific Standard Time (Mexico)" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 10, "Pacific Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 11, "US Mountain Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 12, "Mountain Standard Time (Mexico)" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 13, "Mountain Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 14, "Central America Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 15, "Central Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 9, "UTC-08" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 69, "Namibia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 34, "E. South America Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 36, "Argentina Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 67, "Sudan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 66, "Kaliningrad Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 65, "Israel Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 64, "FLE Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 63, "South Africa Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 62, "West Bank Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 61, "Syria Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 60, "E. Europe Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 59, "Egypt Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 58, "Middle East Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 57, "GTB Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 56, "Jordan Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 55, "W. Central Africa Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 54, "Central European Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 53, "Romance Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 52, "Central Europe Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 51, "W. Europe Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 37, "Greenland Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 38, "Montevideo Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 39, "Magallanes Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 40, "Saint Pierre Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 41, "Bahia Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 42, "UTC-02" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 35, "SA Eastern Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 43, "Mid-Atlantic Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 45, "Cape Verde Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 46, "UTC" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 47, "GMT Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 48, "Greenwich Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 49, "Sao Tome Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 50, "Morocco Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 44, "Azores Standard Time" });

            migrationBuilder.InsertData(
                table: "TimeZoneQueries",
                columns: new[] { "TimeZoneQueryId", "Label" },
                values: new object[] { 139, "Line Islands Standard Time" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrentTimeQueries");

            migrationBuilder.DropTable(
                name: "CurrentTimeZoneQueries");

            migrationBuilder.DropTable(
                name: "TimeZoneQueries");
        }
    }
}
