﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Models
{
    public class ClockworkContext : DbContext
    {


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var i = 1;

            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
            {
                modelBuilder.Entity<TimeZoneQueries>().HasData(new TimeZoneQueries { TimeZoneQueryId = i, Label = z.Id });
                i++;
            }
                
        }

        public DbSet<CurrentTimeQuery> CurrentTimeQueries { get; set; }

        public DbSet<CurrentTimeZoneQueries> CurrentTimeZoneQueries { get; set; }

        public DbSet<TimeZoneQueries> TimeZoneQueries { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=clockwork.db");
            optionsBuilder.EnableSensitiveDataLogging(true);

        }
    }

    public class CurrentTimeQuery
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CurrentTimeQueryId { get; set; }
        public DateTime Time { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
    }


    public class CurrentTimeZoneQueries
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CurrentTimeZoneQueryId { get; set; }
        public DateTime ServerTime { get; set; }
        public int TimeZoneId { get; set; }
        public DateTime TimeZoneTime { get; set; }

        public DateTime UTCTime { get; set; }

        public string ClientIp { get; set; }
    }

    public class TimeZoneQueries
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TimeZoneQueryId { get; set; }

        public string Label { get; set; }
    }
}
